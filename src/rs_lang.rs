use std::fmt::Write;

use crate::spec::{BitmapDef, EnumDef, IncludeDef, OutFileDef, StructDef, Typ};

use convert_case::{Case, Casing};
use log::{debug, trace};

#[derive(Copy, Clone)]
struct Indentation(usize);

/// Convert an IDL type into its Rust type.
fn typ_rs(typ: &Typ) -> String {
    match typ {
        Typ::Ptr(x) => format!("crate::Align64<*mut {}>", typ_rs(x)),
        Typ::Enum(x) | Typ::Struct(x) | Typ::Bitmap(x) => x.to_case(Case::Pascal),
        Typ::Array(x, len) => format!("[{}; {}]", typ_rs(x), len),
        Typ::U8 => String::from("u8"),
        Typ::U16 => String::from("u16"),
        Typ::U32 => String::from("u32"),
        Typ::U64 => String::from("crate::Align64<u64>"),
        Typ::I8 => String::from("i8"),
        Typ::I16 => String::from("i16"),
        Typ::I32 => String::from("i32"),
        Typ::I64 => String::from("crate::Align64<i64>"),
    }
}

/// Add a comment to a struct or a field.
///
/// * `comment`: Actual comment being written
/// * `ind`: provides the indentation level
fn comment(out: &mut String, comment: &str, ind: Indentation) {
    let spaces = " ".repeat(4 * ind.0);
    for line in comment.split('\n') {
        write!(out, "{spaces}///").unwrap();
        if !line.is_empty() {
            write!(out, " {line}").unwrap();
        }
        writeln!(out).unwrap();
    }
}

fn includegen(out: &mut String, def: &IncludeDef) {
    if def.imports.is_empty() {
        return;
    }

    let refered_pascal: Vec<String> = def
        .imports
        .iter()
        .map(|s| s.to_case(Case::Pascal))
        .collect();

    writeln!(
        out,
        "use super::{}::{{{}}};",
        &def.from,
        refered_pascal.join(",")
    )
    .unwrap();

    writeln!(out).unwrap();
}

/// Write a Rust-compatible struct onto `out`
fn structgen(out: &mut String, def: &StructDef) {
    debug!("struct {}", def.name);

    comment(out, &def.description, Indentation(0));
    writeln!(out, "#[repr(C)]").unwrap();
    writeln!(out, "#[derive(Clone, Debug, Default)]").unwrap();
    write!(out, "pub struct {}", def.name.to_case(Case::Pascal)).unwrap();

    if def.fields.is_empty() {
        // zero-sized struct
        writeln!(out, ";\n").unwrap();
        return;
    }

    writeln!(out, " {{").unwrap();

    for f in &def.fields {
        trace!("  field {} type={:?}", f.name, f.typ);

        comment(out, &f.description, Indentation(1));
        writeln!(
            out,
            "    pub {}: {},",
            f.name.to_case(Case::Snake),
            typ_rs(&f.typ)
        )
        .unwrap();
    }
    writeln!(out, "}}\n").unwrap();
}

/// Write a Rust-compatible enum onto `out`
fn enumgen(out: &mut String, def: &EnumDef) {
    debug!("enum {}", def.name);

    comment(out, &def.description, Indentation(0));

    // If the underlying type is 64bits things get trickier. What we want
    // in that case is to use u64 for the underlying type, but set
    // align(8) on the overall repr. Otherwise 32bit platforms have the wrong
    // alignment.
    let repr: &str = if def.typ == Typ::U64 || def.typ == Typ::I64 {
        "align(8)"
    } else {
        &typ_rs(&def.typ)
    };

    writeln!(out, "#[repr({repr})]").unwrap();
    writeln!(out, "#[derive(Clone, Debug, Default, PartialEq, Eq)]").unwrap();
    writeln!(out, "pub enum {} {{", def.name.to_case(Case::Pascal)).unwrap();
    for (i, f) in def.variants.iter().enumerate() {
        trace!("  variant {}={}", f.name, f.value);

        comment(out, &f.description, Indentation(1));
        if i == 0 {
            writeln!(out, "    #[default]").unwrap();
        }
        writeln!(out, "    {} = {},", f.name.to_case(Case::Pascal), f.value).unwrap();
    }
    writeln!(out, "}}\n").unwrap();
}

/// Write a Rust-compatible bitmap onto `out`.
///
/// NOTE: Uses the bitflags crate underneath
fn bitmapgen(out: &mut String, def: &BitmapDef) {
    debug!("bitmap {}", def.name);

    writeln!(out, "bitflags! {{").unwrap();
    comment(out, &def.description, Indentation(1));

    // If the underlying type is 64bits things get trickier. What we want
    // in that case is to use u64 for the underlying type, but set
    // align(8) on the overall repr.
    let is_64bits = def.typ == Typ::U64 || def.typ == Typ::I64;
    let repr = if is_64bits { "align(8)" } else { "C" };

    writeln!(out, "    #[repr({repr})]").unwrap();
    writeln!(
        out,
        "    #[derive(Copy, Clone, Debug, Default, PartialEq, Eq)]"
    )
    .unwrap();
    writeln!(
        out,
        "    pub struct {}: {} {{",
        def.name.to_case(Case::Pascal),
        if is_64bits {
            "u64".to_owned()
        } else {
            typ_rs(&def.typ)
        }
    )
    .unwrap();

    for f in &def.bits {
        trace!("  shift {}={}", f.name, f.shift);

        comment(out, &f.description, Indentation(2));
        writeln!(
            out,
            "        const {} = 1 << {};",
            f.name.to_case(Case::Pascal),
            f.shift
        )
        .unwrap();
    }
    writeln!(out, "    }}").unwrap();

    writeln!(out, "}}\n").unwrap();
}

pub fn parse(filedef: &OutFileDef) -> String {
    let mut out = String::new();

    writeln!(out, "//! {}", &filedef.name).unwrap();
    writeln!(out, "//!").unwrap();
    writeln!(out, "//! AUTOGENERATED. DO NOT MODIFY").unwrap();
    writeln!(out).unwrap();

    if !filedef.bitmaps.is_empty() {
        writeln!(out, "use bitflags::bitflags;\n").unwrap();
    }

    for def in &filedef.includes {
        includegen(&mut out, def);
    }

    for def in &filedef.bitmaps {
        bitmapgen(&mut out, def);
    }

    for def in &filedef.enums {
        enumgen(&mut out, def);
    }

    for def in &filedef.structs {
        structgen(&mut out, def);
    }

    out
}

use std::fmt::Write;

use crate::spec::{BitmapDef, EnumDef, IncludeDef, OutFileDef, StructDef, Typ};

use convert_case::{Case, Casing};
use log::{debug, error, trace};

#[derive(Copy, Clone)]
struct Indentation(usize);

/// Convert an IDL type into its Rust type.
fn structfield(filedef: &OutFileDef, typ: &Typ, name: &str) -> String {
    match typ {
        Typ::Ptr(x) => format!(
            "XEN_GUEST_HANDLE_64({}) {name}",
            structfield(filedef, x, "")
        ),
        Typ::Struct(x) => format!("struct {x} {name};"),
        Typ::Bitmap(x) => {
            // Dealing with bitfields at the ABI boundary is a
            // pain, so we just use the underlying type instead.
            let Some(e) = filedef.bitmaps.iter().find(|y| *x == y.name) else {
                error!("Can't find bitmap {x}. Typo?");
                trace!("{filedef:#?}");
                std::process::exit(1);
            };
            format!(
                "{} /* See {} */",
                structfield(filedef, &e.typ, name),
                e.name
            )
        }
        Typ::Enum(x) => {
            // C can't use an enum as a field and fix its width. Look for its
            // underlying layout and use that type instead.
            let Some(e) = filedef.enums.iter().find(|y| *x == y.name) else {
                error!("Can't find enum {x}. Typo?");
                std::process::exit(1);
            };
            format!(
                "{} /* See {} */",
                structfield(filedef, &e.typ, name),
                e.name
            )
        }
        Typ::Array(x, len) => format!("{}{name}[{len}]", structfield(filedef, x, "")),
        Typ::U8 => format!("uint8_t {name}"),
        Typ::U16 => format!("uint16_t {name}"),
        Typ::U32 => format!("uint32_t {name}"),
        Typ::U64 => format!("uint64_aligned_t {name}"),
        Typ::I8 => format!("int8_t {name}"),
        Typ::I16 => format!("int16_t {name}"),
        Typ::I32 => format!("int32_t {name}"),
        Typ::I64 => format!("int64_aligned_t {name}"),
    }
}

/// Add a comment to a struct or a field.
fn comment(out: &mut String, comment: &str, ind: Indentation) {
    let spaces = " ".repeat(4 * ind.0);

    if comment.contains('\n') {
        writeln!(out, "{spaces}/*").unwrap();
        for line in comment.split('\n') {
            write!(out, "{spaces} *").unwrap();
            if !line.is_empty() {
                write!(out, " {line}").unwrap();
            }
            writeln!(out).unwrap();
        }
        writeln!(out, "{spaces} */").unwrap();
    } else {
        writeln!(out, "{spaces}/* {comment} */").unwrap();
    }
}

fn includegen(out: &mut String, def: &IncludeDef) {
    if !def.imports.is_empty() {
        comment(
            out,
            &format!("for {}", def.imports.join(",\n    ")),
            Indentation(0),
        );
    }

    writeln!(out, "#include \"{}.h\"", def.from).unwrap();
    writeln!(out).unwrap();
}

/// Write a C-compatible struct onto `out`
fn structgen(out: &mut String, filedef: &OutFileDef, def: &StructDef) {
    debug!("struct {}", def.name);

    comment(out, &def.description, Indentation(0));
    writeln!(out, "struct {} {{", def.name.to_case(Case::Snake)).unwrap();
    for f in &def.fields {
        trace!("  field {} type={:?}", f.name, f.typ);

        comment(out, &f.description, Indentation(1));
        writeln!(out, "    {};", structfield(filedef, &f.typ, &f.name),).unwrap();
    }
    writeln!(out, "}};").unwrap();
    writeln!(out).unwrap();
}

/// Write a C-compatible enum onto `out`
fn enumgen(out: &mut String, def: &EnumDef) {
    debug!("enum {}", def.name);

    comment(out, &def.description, Indentation(0));
    writeln!(out, "enum {} {{", def.name.to_case(Case::Snake)).unwrap();
    for f in &def.variants {
        trace!("  variant {}={}", f.name, f.value);

        comment(out, &f.description, Indentation(1));
        writeln!(
            out,
            "    {}_{} = {},",
            def.name.to_case(Case::UpperSnake),
            f.name.to_case(Case::UpperSnake),
            f.value
        )
        .unwrap();
    }
    writeln!(out, "}};").unwrap();
    writeln!(out).unwrap();
}

/// Write a C-compatible enum onto `out`
fn bitmapgen(out: &mut String, def: &BitmapDef) {
    debug!("bitmap {}", def.name);

    comment(out, &def.description, Indentation(0));
    writeln!(out, "struct {} {{}}; /* GREP FODDER */", def.name).unwrap();
    for f in &def.bits {
        trace!("  shift {}={}", f.name, f.shift);
        comment(out, &f.description, Indentation(0));
        writeln!(
            out,
            "#define {}_{} (1U{} << {})",
            def.name.to_case(Case::UpperSnake),
            f.name.to_case(Case::UpperSnake),
            if def.typ == Typ::U64 { "LL" } else { "" },
            f.shift
        )
        .unwrap();
    }
    writeln!(out).unwrap();
}

pub fn parse(filedef: &OutFileDef) -> String {
    let mut out = String::new();
    let name = filedef
        .name
        .from_case(Case::Kebab)
        .to_case(Case::UpperSnake);

    writeln!(out, "/*").unwrap();
    writeln!(out, " * {}", filedef.name).unwrap();
    writeln!(out, " *").unwrap();
    writeln!(out, " * AUTOGENERATED. DO NOT MODIFY").unwrap();
    writeln!(out, " */").unwrap();
    writeln!(out, "#ifndef __XEN_AUTOGEN_{name}_H").unwrap();
    writeln!(out, "#define __XEN_AUTOGEN_{name}_H").unwrap();
    writeln!(out, "#include <stdint.h>\n").unwrap();

    for def in &filedef.includes {
        includegen(&mut out, def);
    }

    for def in &filedef.enums {
        enumgen(&mut out, def);
    }

    for def in &filedef.bitmaps {
        bitmapgen(&mut out, def);
    }

    for def in &filedef.structs {
        structgen(&mut out, filedef, def);
    }

    writeln!(out, "#endif /* __XEN_AUTOGEN_{name}_H */\n").unwrap();

    out
}

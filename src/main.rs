//! CLI tool to generate structs in different languages from specially
//! crafted TOML files. The format of these files follows the following
//! rules.
mod spec;

mod c_lang;
mod rs_lang;

use std::{io::Write, path::PathBuf};

use clap::Parser;
use convert_case::{Case, Casing};
use env_logger::Env;
use log::{error, info};
use spec::OutFileDef;

/// A CLI tool to automatically generate struct definitions in several languages
///
/// It generates a single file for each
#[derive(Parser, Debug)]
#[command(version, about)]
struct Cli {
    /// Path to the input directory containing the hypercall specification
    #[arg(short, long)]
    indir: PathBuf,
    /// Path to the output directory for the generated bindings.
    #[arg(short, long)]
    outdir: PathBuf,
    /// Target language for the contents of `outdir`.
    #[arg(short, long, value_enum)]
    lang: Lang,
}

/// Supported target languages
#[derive(clap::ValueEnum, Clone, Debug)]
#[clap(rename_all = "kebab_case")]
enum Lang {
    #[doc(hidden)]
    C,
    #[doc(hidden)]
    Rust,
}

fn main() {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let cli = Cli::parse();
    info!("args: {:?}", cli);

    let specification = match spec::Spec::new(&cli.indir) {
        Ok(x) => x,
        Err(spec::Error::Toml(x)) => {
            error!("TOML parsing error:");
            error!("{x:#?}");
            std::process::exit(1);
        }
        Err(spec::Error::Io(x)) => {
            error!("IO error:");
            error!("{x:#?}");
            std::process::exit(1);
        }
    };

    let (extension, parser): (&str, fn(&OutFileDef) -> String) = match cli.lang {
        Lang::Rust => (".rs", rs_lang::parse),
        Lang::C => (".h", c_lang::parse),
    };

    if let Err(x) = std::fs::create_dir_all(&cli.outdir) {
        error!("Can't create outdir {:?}: {x}", cli.outdir);
        std::process::exit(1);
    }

    for outfile in &specification.0 {
        let mut path = cli.outdir.clone();
        let name = outfile.name.from_case(Case::Kebab).to_case(Case::Snake);
        path.push(format!("{name}{extension}"));

        info!("Generating {path:?}");

        // Parse the input file before creating the output
        let output = parser(outfile);

        let Ok(mut file) = std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .open(path)
        else {
            error!("Can't open {}", outfile.name);
            std::process::exit(1);
        };

        file.write_all(output.as_bytes()).unwrap();
    }
}
